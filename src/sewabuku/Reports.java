package sewabuku;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import sewabuku.ExportToExcel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Reports {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/sewabuku";
	static final String USER = "root";
	static final String PASS = "";
	
	static Connection conn;
	
	public Reports(String table) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			String sql = "SELECT * FROM "+table;
			JasperDesign jdesign = JRXmlLoader.load("D:\\KULIAH\\SEMESTER 4\\.UAS\\TubesPBO\\res\\"+table+"_report.jrxml");

			JRDesignQuery updateQuery = new JRDesignQuery();
			updateQuery.setText(sql);
			jdesign.setQuery(updateQuery);

			JasperReport Jreport = JasperCompileManager.compileReport(jdesign);
			JasperPrint JasperPrint = JasperFillManager.fillReport(Jreport, null, conn);

			JasperViewer.viewReport(JasperPrint, false);
			
			if(table.equals("buku")) {
				ExportToExcel excel = new ExportToExcel(DataBuku.tableBuku, new File("DataBuku.xls"));
			} else {
				ExportToExcel excel = new ExportToExcel(DataSewa.tableSewa, new File("DataSewa.xls"));
			}
		} 
		catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}